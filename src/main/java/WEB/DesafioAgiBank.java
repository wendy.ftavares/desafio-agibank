package WEB;

import static java.lang.System.setProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DesafioAgiBank {
	private WebDriver driver;

	@Before
	public void setUp() {
		setProperty("webdriver.chrome.driver",
				"/Users/ender/eclipse-workspace/ProjetoAgiBank/src/test/resources/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	@Test
	public void PesquisarSemTermo() throws InterruptedException {

		// Dado que entro no blog do AgiBank
		driver.get("https://blogdoagi.com.br/");
		assertEquals("Blog Do Agi - Agibank", driver.getTitle()); 
		// E clico na Lupa
	    driver.findElement(By.id("search-open")).click();
		// Quando clico no botão Pesquisar
	    driver.findElement(By.cssSelector(".desktop-search .search-submit")).click();
		// Então o site vai retornar varios artigos
	    assertTrue(driver.findElement(By.linkText("Ranking Nacional “As Melhores da Dinheiro”")).getText().startsWith("Ranking"));
	}
	
	@Test
	public void PesquisarOTermoPokemon() throws InterruptedException {

		// Dado que entro no blog do AgiBank
		driver.get("https://blogdoagi.com.br/");
		assertEquals("Blog Do Agi - Agibank", driver.getTitle()); 
		// E clico na Lupa
	    driver.findElement(By.id("search-open")).click();
		// Quando eu pesquiso o termo "Pokemon"
	    driver.findElement(By.name("s")).sendKeys("pokemon");
	    driver.findElement(By.cssSelector(".desktop-search .search-submit")).click();
		// Então o site vai retornar a mensagem
	    assertTrue(driver.findElement(By.cssSelector(".entry-title")).getText().startsWith("Nenhum"));
	}
	
	@Test
	public void PesquisarOTermoBanco() throws InterruptedException {

		// Dado que entro no blog do AgiBank
		driver.get("https://blogdoagi.com.br/");
		assertEquals("Blog Do Agi - Agibank", driver.getTitle()); 
		// E clico na Lupa
	    driver.findElement(By.id("search-open")).click();
		// Quando eu pesquiso o termo "Pokemon"
	    driver.findElement(By.name("s")).sendKeys("banco");
	    driver.findElement(By.cssSelector(".desktop-search .search-submit")).click();
		// Então o site vai retornar a mensagem
	    assertEquals("Resultados da busca por: banco", driver.findElement(By.xpath("//*[@id=\"primary\"]/header/h1")).getText());
	}

	@After
	public void tearDown() {
		driver.quit();
	}

}
